/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef IO_VISIONSDK_KERNELS_INTERFACESMAP_HPP_  // NOLINT
#define IO_VISIONSDK_KERNELS_INTERFACESMAP_HPP_

namespace Io {
    namespace VisionSDK {
        namespace Kernels {
            namespace Enums {
                enum KernelType : int;
            }
            namespace Example {
                class ExampleKernel;
            }
        }
    }
}

#endif  // IO_VISIONSDK_KERNELS_INTERFACESMAP_HPP_  // NOLINT
