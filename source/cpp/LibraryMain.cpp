/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "reference.hpp"

static vx_kernel_description_t *kernels[] = {
        &ExampleKernel
};

static vx_uint32 numKernels = sizeof(kernels) / sizeof(kernels[0]);

VX_API_ENTRY vx_status VX_API_CALL vxPublishKernels(vx_context $context) {
    vx_status status = vx_status_e::VX_FAILURE;
    vx_uint32 p = 0, k = 0;
    for (k = 0; k < numKernels; k++) {
        vx_kernel kernel = nullptr;

        kernel = vxAddUserKernel($context,
                                 kernels[k]->name,
                                 kernels[k]->enumeration,
                                 kernels[k]->function,
                                 kernels[k]->numParams,
                                 kernels[k]->validate,
                                 kernels[k]->initialize,
                                 kernels[k]->deinitialize);
        if (kernel != nullptr) {
            status = vx_status_e::VX_SUCCESS;
            for (p = 0; p < kernels[k]->numParams; p++) {
                status = vxAddParameterToKernel(kernel, p,
                                                kernels[k]->parameters[p].direction,
                                                kernels[k]->parameters[p].data_type,
                                                kernels[k]->parameters[p].state);
                if (status != vx_status_e::VX_SUCCESS) {
                    vxAddLogEntry((vx_reference)$context, status, "Failed to add parameter %d to kernel %s! (%d)\n", p, kernels[k]->name,
                                  status);
                    break;
                }
            }
            if (status == vx_status_e::VX_SUCCESS) {
                status = vxFinalizeKernel(kernel);
                if (status != vx_status_e::VX_SUCCESS) {
                    vxAddLogEntry((vx_reference)$context, status, "Failed to finalize kernel[%u]=%s\n", k, kernels[k]->name);
                }
            } else {
                status = vxRemoveKernel(kernel);
                if (status != vx_status_e::VX_SUCCESS) {
                    vxAddLogEntry((vx_reference)$context, status, "Failed to remove kernel[%u]=%s\n", k, kernels[k]->name);
                }
            }
        } else {
            vxAddLogEntry((vx_reference)$context, status, "Failed to add kernel %s\n", kernels[k]->name);
        }
    }
    return status;
}

VX_API_ENTRY vx_status VX_API_CALL vxUnpublishKernels(vx_context $context) {
    vx_status status = vx_status_e::VX_FAILURE;

    vx_uint32 k = 0;
    for (k = 0; k < numKernels; k++) {
        vx_kernel kernel = vxGetKernelByName($context, kernels[k]->name);
        vx_kernel kernelCpy = kernel;

        if (kernel != nullptr) {
            status = vxReleaseKernel(&kernelCpy);
            if (status != vx_status_e::VX_SUCCESS) {
                vxAddLogEntry((vx_reference)$context, status, "Failed to release kernel[%u]=%s\n", k, kernels[k]->name);
            } else {
                kernelCpy = kernel;
                status = vxRemoveKernel(kernelCpy);
                if (status != vx_status_e::VX_SUCCESS) {
                    vxAddLogEntry((vx_reference)$context, status, "Failed to remove kernel[%u]=%s\n", k, kernels[k]->name);
                }
            }
        } else {
            vxAddLogEntry((vx_reference)$context, status, "Failed to get added kernel %s\n", kernels[k]->name);
        }
    }

    return status;
}
