/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_KERNELS_HPP_  // NOLINT
#define IO_VISIONSDK_KERNELS_HPP_

#include "../../dependencies/io-vision-sdk-xcppcommons/source/cpp/reference.hpp"

#include <VX/vx_helper.h>

// global-using-start
using std::string;
// global-using-stop

#include "interfacesMap.hpp"
#include "Io/VisionSDK/Kernels/sourceFilesMap.hpp"

#include "Io/VisionSDK/Kernels/Enums/KernelType.hpp"

// generated-code-start
#include "Io/VisionSDK/Kernels/Example/ExampleKernel.hpp"
#include "VxKernels.hpp"
// generated-code-end

#endif  // IO_VISIONSDK_KERNELS_HPP_  NOLINT
