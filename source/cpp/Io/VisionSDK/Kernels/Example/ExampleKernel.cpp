/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Io::VisionSDK::Kernels::Example::ExampleKernel;

static vx_param_description_t exampleNodeKernelParams[] = {
        {vx_direction_e::VX_INPUT,  vx_type_e::VX_TYPE_IMAGE,  vx_parameter_state_e::VX_PARAMETER_STATE_REQUIRED},
        {vx_direction_e::VX_INPUT,  vx_type_e::VX_TYPE_SCALAR, vx_parameter_state_e::VX_PARAMETER_STATE_REQUIRED},
        {vx_direction_e::VX_OUTPUT, vx_type_e::VX_TYPE_IMAGE,  vx_parameter_state_e::VX_PARAMETER_STATE_REQUIRED},
};

static vx_status VX_CALLBACK exampleKernel(vx_node $node, const vx_reference $parameters[], vx_uint32 $parametersCount) {
    vx_status status = vx_status_e::VX_ERROR_INVALID_PARAMETERS;

    if ($node != nullptr && $parameters != nullptr && $parametersCount == dimof(exampleNodeKernelParams)) {
        auto image = (vx_image)$parameters[0];
        auto scalar = (vx_scalar)$parameters[1];
        auto output = (vx_image)$parameters[2];

        // TODO(nxa33894) Write your implementation here.

        status = vx_status_e::VX_SUCCESS;
    }

    return status;
}

static vx_status VX_CALLBACK setExampleValidRectangle(vx_node $node, vx_uint32 $index, const vx_rectangle_t *const *$inputValid,
                                                      vx_rectangle_t *const *$outputValid) {
    vx_status status = vx_status_e::VX_ERROR_INVALID_PARAMETERS;

    if ($node != nullptr && $index < dimof(exampleNodeKernelParams) && $inputValid != nullptr && $outputValid != nullptr) {
        $outputValid[0]->start_x = $inputValid[0]->start_x;
        $outputValid[0]->start_y = $inputValid[0]->start_y;
        $outputValid[0]->end_x = $inputValid[0]->end_x;
        $outputValid[0]->end_y = $inputValid[0]->end_y;
        status = vx_status_e::VX_SUCCESS;
    }

    return status;
}

static vx_status VX_CALLBACK exampleValidate(vx_node $node, const vx_reference $parameters[], vx_uint32 $parametersCount,
                                             vx_meta_format $meta[]) {
    vx_status status = vx_status_e::VX_ERROR_INVALID_PARAMETERS;

    if ($node != nullptr && $parametersCount == dimof(exampleNodeKernelParams) && $meta != nullptr) {
        vx_parameter param1 = nullptr;
        vx_parameter param2 = nullptr;

        vx_image input = nullptr;
        vx_scalar shift = nullptr;

        param1 = vxGetParameterByIndex($node, 0);
        param2 = vxGetParameterByIndex($node, 1);

        if (vx_status_e::VX_SUCCESS == vxGetStatus((vx_reference)param1) &&
            vx_status_e::VX_SUCCESS == vxGetStatus((vx_reference)param2)) {
            /* validate input image */
            status = vxQueryParameter(param1, vx_parameter_attribute_e::VX_PARAMETER_REF, &input, sizeof(input));
            if (vx_status_e::VX_SUCCESS == status &&
                vx_status_e::VX_SUCCESS == vxGetStatus((vx_reference)input)) {
                vx_df_image format = 0;
                status |= vxQueryImage(input, vx_image_attribute_e::VX_IMAGE_FORMAT, &format, sizeof(format));
                if (vx_status_e::VX_SUCCESS == status) {
                    if (format != vx_df_image_e::VX_DF_IMAGE_U8) {
                        status = vx_status_e::VX_ERROR_INVALID_FORMAT;
                    }
                }
            }

            /* validate input shift */
            status |= vxQueryParameter(param2, vx_parameter_attribute_e::VX_PARAMETER_REF, &shift, sizeof(shift));
            if (vx_status_e::VX_SUCCESS == status &&
                vx_status_e::VX_SUCCESS == vxGetStatus((vx_reference)shift)) {
                vx_enum type = 0;
                status |= vxQueryScalar(shift, vx_scalar_attribute_e::VX_SCALAR_TYPE, &type, sizeof(type));
                if (vx_status_e::VX_SUCCESS == status) {
                    if (type != vx_type_e::VX_TYPE_UINT32) {
                        status = vx_status_e::VX_ERROR_INVALID_TYPE;
                    }
                }
            }

            /* validate output image */
            if (vx_status_e::VX_SUCCESS == status) {
                vx_uint32 srcWidth = 0;
                vx_uint32 srcHeight = 0;
                vx_df_image dstFormat = vx_df_image_e::VX_DF_IMAGE_U8;
                vx_kernel_image_valid_rectangle_f callback = &setExampleValidRectangle;

                status |= vxQueryImage(input, vx_image_attribute_e::VX_IMAGE_WIDTH, &srcWidth, sizeof(srcWidth));
                status |= vxQueryImage(input, vx_image_attribute_e::VX_IMAGE_HEIGHT, &srcHeight, sizeof(srcHeight));

                status |= vxSetMetaFormatAttribute($meta[2], vx_image_attribute_e::VX_IMAGE_WIDTH, &srcWidth, sizeof(srcWidth));
                status |= vxSetMetaFormatAttribute($meta[2], vx_image_attribute_e::VX_IMAGE_HEIGHT, &srcHeight, sizeof(srcHeight));
                status |= vxSetMetaFormatAttribute($meta[2], vx_image_attribute_e::VX_IMAGE_FORMAT, &dstFormat, sizeof(dstFormat));

                status |= vxSetMetaFormatAttribute($meta[2], vx_meta_valid_rect_attribute_e::VX_VALID_RECT_CALLBACK, &callback,
                                                   sizeof(callback));
            }
        }

        if (input != nullptr) {
            vxReleaseImage(&input);
        }
        if (shift != nullptr) {
            vxReleaseScalar(&shift);
        }

        if (param1 != nullptr) {
            vxReleaseParameter(&param1);
        }
        if (param2 != nullptr) {
            vxReleaseParameter(&param2);
        }
    }

    return status;
}

static vx_status VX_CALLBACK exampleInitialize(vx_node $node, const vx_reference $parameters[], vx_uint32 $parametersCount) {
    vx_status status = vx_status_e::VX_SUCCESS;

    // TODO(nxa33894) Write initialization routine here.

    return status;
}

static vx_status VX_CALLBACK exampleDeinitialize(vx_node $node, const vx_reference $parameters[], vx_uint32 $parametersCount) {
    vx_status status = vx_status_e::VX_SUCCESS;

    // TODO(nxa33894) Write cleanup routine here.

    return status;
}

vx_kernel_description_t ExampleKernel = {
        Io::VisionSDK::Kernels::Enums::KernelType::VX_KERNEL_EXAMPLE,
        "io.vision.sdk.kernels.example",
        exampleKernel,
        exampleNodeKernelParams, dimof(exampleNodeKernelParams),
        exampleValidate,
        nullptr,
        nullptr,
        exampleInitialize,
        exampleDeinitialize
};
