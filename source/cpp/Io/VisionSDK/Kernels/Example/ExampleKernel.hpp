/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_KERNELS_EXAMPLE_EXAMPLEKERNEL_HPP_
#define IO_VISIONSDK_KERNELS_EXAMPLE_EXAMPLEKERNEL_HPP_

/**
 * Enter some algorithm description here.
 */
class Io::VisionSDK::Kernels::Example::ExampleKernel {
 public:
    // TODO(nxa33894) To be defined. Use private-internal static method with external variable defined in LibraryMain.cpp
};

#endif  // IO_VISIONSDK_KERNELS_EXAMPLE_EXAMPLEKERNEL_HPP_
